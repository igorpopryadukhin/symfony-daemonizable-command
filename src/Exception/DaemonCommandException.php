<?php
namespace SDC\Exception;

use Exception;

/**
 *
 * @author Igor Popryadukhin <igorpopryadukhin@gmail.com>
 */
class DaemonCommandException extends Exception
{

}