<?php

namespace SDC;

use InvalidArgumentException;
use LogicException;
use SDC\Exception\DaemonCommandException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

/**
 *
 * @author Igor Popryadukhin <igorpopryadukhin@gmail.com>
 */
abstract class DaemonCommand extends Command
{
    public const DEFAULT_TIMEOUT = 5;

    private $code;
    private $timeout;
    private $returnCode;
    private $shutdownRequested;
    private $lastUsage;
    private $lastPeakUsage;

    /**
     * @param string|null $name
     */
    public function __construct(string $name = null)
    {
        $this->shutdownRequested = false;
        $this->setTimeout(static::DEFAULT_TIMEOUT);
        $this->returnCode = 0;
        $this->lastUsage = 0;
        $this->lastPeakUsage = 0;

        parent::__construct($name);

        $this->addOption("run-once",
            null,
            InputOption::VALUE_NONE,
            "Выполнить один раз."
        );
        $this->addOption("detect-leaks",
            null,
            InputOption::VALUE_NONE,
            "Показать информацию о потреблении памяти"
        );

        // Установите наш цикл как исполняемый код
        parent::setCode([$this, "loop"]);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     * @throws \Exception
     */
    public function run(InputInterface $input, OutputInterface $output): int
    {
        // Добавить обработчик сигнала
        if (function_exists("pcntl_signal")) {
            // Включить асинхронные сигналы для быстрой обработки сигналов
            try {
                pcntl_async_signals(true);
            } catch (Throwable $e) {
                declare(ticks=1);
            }

            pcntl_signal(SIGTERM, [$this, "handleSignal"]);
            pcntl_signal(SIGINT, [$this, "handleSignal"]);
        }

        return parent::run($input, $output);
    }

    /**
     * Обработка сигналов процесса.
     *
     * @param int $signal Код сигнала для обработки.
     */
    public function handleSignal(int $signal): void
    {
        switch ($signal) {
            case SIGTERM:
            case SIGINT:
                $this->shutdown();
                break;
        }
    }

    /**
     *
     * @param InputInterface $input Экземпляр InputInterface
     * @param OutputInterface $output Экземпляр OutputInterface
     *
     * @return integer Код выхода команды
     *
     * @throws \Exception
     */
    protected function loop(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->starting($input, $output);

            do {
                // Старт итерации.
                $this->startIteration($input, $output);

                // Выполнить команду
                $this->execute($input, $output);

                // Завершить текущую итерацию
                $this->finishIteration($input, $output);

                // Запросить выключение, если нужно запустить только один раз
                if ((bool)$input->getOption("run-once")) {
                    $this->shutdown();
                }

                // Print memory report if requested
                if ($input->getOption("detect-leaks")) {
                    // Получить информацию о памяти
                    $peak = $this->getMemoryInfo(true);
                    $curr = $this->getMemoryInfo(false);

                    // Распечатать отчет
                    $output->writeln("== ИСПОЛЬЗОВАНИЕ ПАМЯТИ ==");
                    $output->writeln(sprintf("Пик: %.02f KByte <%s>%s (%.03f %%)</%s>", $peak["amount"] / 1024,
                        $peak["statusType"], $peak["statusDescription"], $peak["diffPercentage"], $peak["statusType"]));
                    $output->writeln(sprintf("Текущий.: %.02f KByte <%s>%s (%.03f %%)</%s>", $curr["amount"] / 1024,
                        $curr["statusType"], $curr["statusDescription"], $curr["diffPercentage"], $curr["statusType"]));
                    $output->writeln("");

                    // Удалить переменные, чтобы предотвратить использование памяти
                    unset($peak, $curr);
                }

                // Спать некоторое время, обратите внимание, что сон будет прерван сигналом
                if (!$this->shutdownRequested) {
                    usleep($this->timeout);
                }
            } while (! $this->shutdownRequested);
        } catch (DaemonCommandException $ignore) {
        }

        // Вызов финального метода
        $this->finalize($input, $output);

        return $this->returnCode;
    }

    /**
     * Вызывается перед первым выполнением
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function starting(InputInterface $input, OutputInterface $output): void
    {
    }

    /**
     * Вызывается перед каждой итерацией
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function startIteration(InputInterface $input, OutputInterface $output): void
    {
    }

    /**
     * Вызывается после каждой итерации
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function finishIteration(InputInterface $input, OutputInterface $output): void
    {
    }

    /**
     * Получить информацию о текущем использовании памяти
     *
     * @param bool Истина для пикового использования, ложь для текущего использования
     *
     * @return array
     */
    private function getMemoryInfo(bool $peak = false): array
    {
        $lastUsage = ($peak) ? $this->lastPeakUsage : $this->lastUsage;
        $info["amount"] = ($peak) ? memory_get_peak_usage() : memory_get_usage();
        $info["diff"] = $info["amount"] - $lastUsage;
        $info["diffPercentage"] = ($lastUsage == 0) ? 0 : $info["diff"] / ($lastUsage / 100);
        $info["statusDescription"] = "stable";
        $info["statusType"] = "info";

        if ($info["diff"] > 0) {
            $info["statusDescription"] = "increasing";
            $info["statusType"] = "error";
        } else {
            if ($info["diff"] < 0) {
                $info["statusDescription"] = "decreasing";
                $info["statusType"] = "comment";
            }
        }

        // Обновить переменные последнего использования
        if ($peak) {
            $this->lastPeakUsage = $info["amount"];
        } else {
            $this->lastUsage = $info["amount"];
        }

        return $info;
    }

    /**
     * @param callable $code
     *
     * @return $this
     */
    public function setCode(callable $code): DaemonCommand
    {
        // Точная копия нашего родителя
        // Убедитесь, что у нас есть доступ для вызова его на каждой итерации.
        if (! is_callable($code)) {
            throw new InvalidArgumentException("Invalid callable provided to Command::setCode.");
        }

        $this->code = $code;

        return $this;
    }

    /**
     * Логика исполнения.
     *
     * Этот метод будет вызываться на каждой итерации.
     * Постарайтесь сделать это быстро, обрабатывайте только одну единицу на каждой итерации.
     * Если один из модулей является неэффективным (например, из-за сети),
     * обрабатывайте небольшие партии и вызывайте throwExceptionOnShutdown всякий раз,
     * когда можете. Это предотвращает неожиданное завершение процесса и ускоряет завершение работы.
     *
     * @param InputInterface $input Экземпляр InputInterface
     * @param OutputInterface $output Экземпляр OutputInterface
     *
     * @return int 0, если все прошло нормально, или код выхода
     *
     * @throws LogicException Когда этот абстрактный метод не реализован
     * @see    setCode()
     */
    protected function  execute(InputInterface $input, OutputInterface $output): int
    {
        return parent::execute($input, $output);
    }

    /**
     * Установите тайм-аут этой команды.
     *
     * @param int|float $timeout Тайм-аут между двумя итерациями в секундах
     *
     * @return Command Текущий экземпляр
     *
     * @throws \InvalidArgumentException
     */
    public function setTimeout(float $timeout): Command
    {
        if ($timeout < 0) {
            throw new InvalidArgumentException("Invalid timeout provided to Command::setTimeout.");
        }

        $this->timeout = (int) (1000000 * $timeout);

        return $this;
    }

    /**
     * Получите тайм-аут этой команды.
     *
     * @return float Тайм-аут между двумя итерациями в секундах
     */
    public function getTimeout(): float
    {
        return ($this->timeout / 1000000);
    }

    /**
     * Установите код возврата этой команды.
     *
     * @param int 0, если все прошло нормально, или код ошибки
     *
     * @return Command Текущий экземпляр
     *
     * @throws \InvalidArgumentException
     */
    public function setReturnCode(int $returnCode): Command
    {
        if ($returnCode < 0) {
            throw new InvalidArgumentException("Invalid returnCode provided to Command::setReturnCode.");
        }

        $this->returnCode = $returnCode;

        return $this;
    }

    /**
     * Получите код возврата этой команды.
     *
     * @return int 0, если все прошло нормально, или код ошибки
     */
    public function getReturnCode(): int
    {
        return $this->returnCode;
    }

    /**
     * Дайте команду команде корректно завершить бесконечный цикл.
     *
     * Это завершит текущую итерацию и даст команде возможность выполнить очистку.
     *
     * @return Command The current instance
     */
    public function shutdown()
    {
        $this->shutdownRequested = true;

        return $this;
    }

    /**
     * Проверяет, запрошено ли завершение работы, и выдает исключение, если да.
     *
     * Может использоваться для (добровольного) выхода из цикла выполнения во время выполнения,
     * используйте это, если выполнение кода выполнения занимает довольно много времени в точке,
     * где вы все еще можете выйти, не повредив какие-либо данные.
     *
     * @return Command Текущий экземпляр
     *
     * @throws \SDC\Exception\DaemonCommandException
     */
    protected function throwExceptionOnShutdown(): Command
    {
        // Убедитесь, что все сигналы обрабатываются
        if (function_exists("pcntl_signal_dispatch")) {
            pcntl_signal_dispatch();
        }

        if ($this->shutdownRequested) {
            throw new DaemonCommandException("Volunteered to break out of the EndlessCommand runloop because a shutdown is requested.");
        }

        return $this;
    }

    /**
     * Вызывается при завершении работы после завершения последней итерации.
     *
     * Используйте это, чтобы произвести некоторую очистку, но делайте это быстро.
     * Если вы потратите слишком много времени, и мы должны выйти из-за изменения сигнала,
     * процесс будет остановлен! Это аналог initialize().
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance, will be a NullOutput if the verbose is not set
     */
    protected function finalize(InputInterface $input, OutputInterface $output): void
    {
    }
}